variable "url" {
    description = "GitLab URL"
}

variable "accesstoken" {
    description = "GitLab Access Token"
}

variable "check_label" {
    description = "Label name to be checked"
}

variable "resthook_response_issueid" {
    default = ""
    description = "Extract IssueID from RESTHOOK RESPONSE"
}

#########################################################
# Poll GitLab Issues for approval status
#########################################################
resource "null_resource" "poll_endpoint" {
 provisioner "local-exec" {

    command = "/bin/bash pollGitLabIssue.sh $URL $ACCESSTOKEN $LABEL $ISSUEID"
    environment = {
      URL           = var.url
      ACCESSTOKEN   = var.accesstoken
      LABEL         = var.check_label
      ISSUEID       = var.resthook_response_issueid
    }
  }
}

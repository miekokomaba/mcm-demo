# provider variables

variable "url" {
  description = "GitLab URL"
  default = "https://gitlab.com/api/v4/projects/30643119/issues/"
}

variable "accesstoken" {
  description = "GitLab Access Token"
  default = "${var.gitlab_access_token}"
}

variable "check_label" {
  description = "Label name to be checked"
  default = "${var.gitlab_label}"
}

variable "resthook_response_issueid" {
  description = "Extract IssueID from RESTHOOK RESPONSE"
  default = "${resthooks.<ID>.output.body.iid}"
}

#!/bin/sh

SetParams() {
   URL=$1
   AUTH=$2
   LABEL=$3
   ISSUSEID=$4

   # Log params
   echo "GitLab URL: $URL"
   echo "Check LABEL: $LABEL"
   echo "IssueID: $ISSUSEID"
   
}

PollGitLabIssue() {
   # Set params
   SetParams $1 $2 $3 $4

   count=0
   while true
   do
      ## POLLING GILLAB URL
      let ++count
      result=$(curl -X GET -k -sS -H "Authorization: Bearer $AUTH" $URL/$ISSUEID | jq ".labels")
      printf "`date "+%Y/%m/%d %H:%M:%S"` ${count}:Checking Label status %s\n"
      if [ "`echo $result | grep $LABEL`" ]; then
         echo  "Approved Label: $result \n"
         exit 0
      fi
      if [ ${count} -eq 60 ];then
         printf "Timed-out Label:  $result \n"
         exit 1
      fi
      sleep 30
   done
}

PollGitLabIssue $1 $2 $3 $4

# provider variables

variable "provider_region" {
  description = "Provider region"
  default = "ap-northeast-1"
}

variable "secret_access_key" {
  description = "Provider access key"
  default = "AWS_ACCESS_KEY"
}

variable "secret_key" {
  description = "Provider secret key"
  default = "AWS_SECRET_KEY"
}

# instance web variables
variable "server_instance_type" {
  description = "Server instance type"
  default = "t2.micro"
}

variable "server_tag_name" {
  description = "Server tag name"
  default = "aws-tf-web"
}


variable "ssh_public_key" {
  description = "SSH public key"
  default = "SSH_PUBLIC_KEY"
}
